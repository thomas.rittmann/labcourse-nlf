% set(groot, 'defaultAxesTickLabelInterpreter','latex');
% set(groot, 'defaultLegendInterpreter','latex');
% set(groot, 'defaultTextInterpreter','none');
% 
% 
% set(0,'defaultfigureposition',[610   428   700   525])
set(0,'defaultfigureposition',[610   428   900   525])
%% Output Laser diode (abs. units)
DataDiode=csvread('laserdiode_characterization.csv',1,0);
DataDiode(:,3)=DataDiode(:,3)*392/interp1(DataDiode(:,1),DataDiode(:,3),550); %calibration

z0=1:8;
z1=9:15;
p=polyfit(DataDiode(z1,1),DataDiode(z1,3),1);
onset=-p(2)/p(1);
x=[100 550];
y=polyval(p,x);

z2=1:29;
z2=setdiff(z2,[z0,z1]);
p2=polyfit(DataDiode(z2,1),DataDiode(z2,3),1);
x2=[30 600];
y2=polyval(p2,x2);

h=figure;
hold on, box on
% errorbar(DataDiode(z0,1),DataDiode(z0,3),0.1*ones(size(z0)),'ko','MarkerFaceColor','k')
plot(DataDiode(z0,1),DataDiode(z0,3),'ko','MarkerFaceColor','k')
plot(DataDiode(z1,1),DataDiode(z1,3),'rs','MarkerFaceColor','r')
plot(DataDiode(z2,1),DataDiode(z2,3),'bo','MarkerFaceColor','b')
plot(x,y,'r')
plot(x2,y2,'b')
xlim([0 600])
ylim([-30 450])
xlabel('Current (mA)')
ylabel('Output (mW)')
% title('Laser diode')
text(30,420,sprintf('At %.1f�C',DataDiode(1,2)))
text(180,0,sprintf('Threshold: %.0f mA',round(onset,-1)),'color','r')
text(330,250,sprintf('%.2f W/A',p(1)),'color','r','rotation',45)
text(440,270,sprintf('%.2f W/A',p2(1)),'color','b','rotation',30)
set(gca,'YTick',0:100:500)
set(h,'Units','Inches');
pos = get(h,'Position');
set(h,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
print(h,'..\LaserDiode','-dpdf','-r600')


%% Temperature dependence of laser diode from absorption measurements
Abs250mA=csvread('TempDependence250mA.csv',1,0);
Abs400mA=csvread('TempDependence400mA.csv',1,0);
Abs550mA=csvread('TempDependence550mA.csv',1,0);

% Abs250mA(:,2)=Abs250mA(:,2)/5; % gain factor changed

%Laser diode power corrected
% Abs400mA(:,2)=Abs400mA(:,2)/1.5;
% Abs550mA(:,2)=Abs550mA(:,2)/3;

c=lines(3);

figure
hold on, box on
plot(Abs250mA(:,1),Abs250mA(:,2),'d-','color',c(1,:),'MarkerFaceColor',c(1,:))
plot(Abs400mA(:,1),Abs400mA(:,2),'o-','color',c(2,:),'MarkerFaceColor',c(2,:))
plot(Abs550mA(:,1),Abs550mA(:,2),'s-','color',c(3,:),'MarkerFaceColor',c(3,:))
xlim([35 45])
legend({'250mA','400mA','550mA'},'location','best')
xlabel('Temperature (�C)')
ylabel('Intensity (arb.u.)')
title('Absorption measurement')

extrema=[26 37 43];
colorExtrema=['r','r','b'];

h=figure;
hold on, box on
plot(Abs550mA(:,1),Abs550mA(:,2),'s-','color','k','MarkerFaceColor','k')
for ii=1:3
    line(extrema(ii)*[1 1], [1200 Abs550mA(Abs550mA(:,1)==extrema(ii),2)],...
        'color',colorExtrema(ii),'linestyle','--','clipping','off')
    text(extrema(ii)-2.5,800,sprintf('%.0f�C',extrema(ii)),'color',colorExtrema(ii))
end
xlabel('Temperature (�C)')
ylabel('Intensity (arb.u.)')
% title('Absorption measurement')
set(h,'Units','Inches');
pos = get(h,'Position');
set(h,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
print(h,'..\Absorption','-dpdf','-r600')

%% Fluorescence measurement
fluorescenceOff=csvread('fluorescenceOff.csv');
fluorescenceOff2=csvread('fluorescenceOff2.csv');
fluorescenceOn=csvread('fluorescenceOn.csv');

offLimits=[19.2 -20.4];
onLimits=[-18 21.2];

fluorescenceOff(:,2)=fluorescenceOff(:,2)-min(offLimits);
fluorescenceOff2(:,2)=fluorescenceOff2(:,2)-min(offLimits);
fluorescenceOn(:,2)=fluorescenceOn(:,2)-min(onLimits);

expDecay = @(a,b,c,x) a*exp(-1/b*x)+c;
expDecay2= @(b,x) expDecay(abs(diff(offLimits)),b,0,x);
expDecay3= @(b,x) expDecay(-abs(diff(onLimits)),b,abs(diff(onLimits)),x);
fOff=fit(fluorescenceOff(:,1),fluorescenceOff(:,2),expDecay2 ,'StartPoint',50)%[40 250 0])
fOff2=fit(fluorescenceOff2(:,1),fluorescenceOff2(:,2),expDecay2,'StartPoint',50)%[40 250 0])
fOn=fit(fluorescenceOn(:,1),fluorescenceOn(:,2),expDecay3,'StartPoint',50)%[40 250 0])

lifetime=[fOff.b fOff2.b fOn.b];
x=0:10:600;

c=lines(3);
h=figure;
hold on,box on
xlabel('Time (�s)')
ylabel('Voltage (mV)')
% plot(fluorescenceOff2(:,1),fluorescenceOff2(:,2),'d','MarkerFaceColor',c(2,:))
plot(fluorescenceOn(:,1),fluorescenceOn(:,2),'ro','MarkerFaceColor','r')
plot(fluorescenceOff(:,1),fluorescenceOff(:,2),'bs','MarkerFaceColor','b')
legend({'On','Off'},'location','best','box','off')
plot(x,feval(fOn,x),'color','r')
plot(x,feval(fOff,x),'color','b')
% plot(x,feval(fOff2,x),'color',c(2,:))
set(h,'Units','Inches');
pos = get(h,'Position');
set(h,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
print(h,'..\Fluorescence','-dpdf','-r600')
% figure
% semilogy(fluorescenceOff(:,1),fluorescenceOff(:,2)+16)
% hold on
% semilogy(fluorescenceOff2(:,1),fluorescenceOff2(:,2)+16)
% semilogy(fluorescenceOn(:,1),18-fluorescenceOn(:,2))

%% Temp. dependence Nd:YAG laser
LaserTempDep=csvread('LaserTempDependence_edit.csv',1,0);
extrema=[27 38 44];
colorExtrema=['r','r','b'];
h=figure;
plot(LaserTempDep(:,1),LaserTempDep(:,2),'ks-','MarkerFaceColor','k')
for ii=1:3
    line(extrema(ii)*[1 1], [20 LaserTempDep(LaserTempDep(:,1)==extrema(ii),2)],...
        'color',colorExtrema(ii),'linestyle','--','clipping','off')
    text(extrema(ii)-2.5,35,sprintf('%.0f�C',extrema(ii)),'color',colorExtrema(ii))
end
ylim([20 110])
xlabel('Temperature (�C)')
ylabel('Power (mW)')
% hold on
set(h,'Units','Inches');
pos = get(h,'Position');
set(h,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
print(h,'..\LaserTempDependence','-dpdf','-r600')
% plot(Abs550mA(:,1),(max(Abs550mA(:,2))-Abs550mA(:,2))/10,'rs-','MarkerFaceColor','r')

%% Output Nd:Yag laser (absolute units)
Data1064=csvread('LaserClassification.csv',1,0);

p1064=polyfit(Data1064(end-10:end-1,1),Data1064(end-10:end-1,3),1);
x=[180 400];
x2=[400 600];
y=polyval(p1064,x);
y2=polyval(p1064,x2);
ndyag.threshold=-p1064(2)/p1064(1);

p1064_all=polyfit(Data1064(1:end-1,1),Data1064(1:end-1,3),1);
x_all=[180 600];
y_all=polyval(p1064_all,x_all);

h=figure;
hold on
plot(x_all,y_all,'color',0.8*[1 1 1],'linew',4)
plot(x,y,'r','linew',1.5)
% plot(x2,y2,'r--','linew',1.5)
plot(Data1064(:,1),Data1064(:,3),'ks','MarkerFaceColor','k')
text(500,75,sprintf('%.2f W/A',p1064_all(1)),'color',0.7*[1 1 1],'rotation',37)
ylim([0 120])
posAx=get(gca,'position');
xl=xlim;
annotation(h,'textarrow',posAx(1)+posAx(3)*(ndyag.threshold-xl(1))/diff(xl)*[1 1],[0.30 0.13],...
    'String',{'Threshold:',sprintf('%.0f mA',ndyag.threshold)},'color','r','HorizontalAlignment','center');
xlabel('Current (mA)')
ylabel('Power (mW)')
% title('Nd:YAG laser output')

ax1=gca;
ax2=axes('Position',ax1.Position,'XAxisLocation','top','YAxisLocation','right',...
    'YTick',35:2:45,'XTickLabel','','Color','none');
ax2.YColor = 'b';
hold on
plot(Data1064(:,1),Data1064(:,2),'bd','MarkerFaceColor','b','MarkerSize',5)
ylabel('Temperature (�C)')
ylim([36 42])

linkaxes([ax1,ax2],'x')

set(h,'Units','Inches');
pos = get(h,'Position');
set(h,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
print(h,'..\NdYAG-Output','-dpdf','-r600')
%%
figure
plot(interp1(DataDiode(:,1),DataDiode(:,3),Data1064(:,1)),Data1064(:,3),'ks-','MarkerFaceColor','k')
xlabel('Pump power (mW)')
ylabel('Output power (mW)')

fTemp=fit(Data1064(1:20,1),Data1064(1:20,2),'poly1');
figure
hold on, box on
% plot(x,y,'r','linew',1.5)
plot(fTemp,Data1064(1:20,1),Data1064(1:20,2))%,'bs','MarkerFaceColor','b')
xlabel('Current (mA)')
ylabel('Temperature (�C)')
title('Nd:YAG laser output')

%% Output green Nd:Yag laser (absolute units)
DataSHG=csvread('SHG.csv',1,0);
outlyer=DataSHG(:,1)>480;
pSHG=fit(DataSHG(:,1),DataSHG(:,2),'a*(x-b)^2','StartPoint',[1 250],'Exclude',outlyer);
x=pSHG.b:580;
y=feval(pSHG,x);

h=figure;
hold on, box on
plot(DataSHG(:,1),DataSHG(:,2),'ks','MarkerFaceColor','k')
plot(DataSHG(outlyer,1),DataSHG(outlyer,2),'rs','MarkerFaceColor','r')
plot(x,y,'k-')
xlim([180 570])
posAx=get(gca,'position');
xl=xlim;
annotation(h,'textarrow',posAx(1)+posAx(3)*(pSHG.b-xl(1))/diff(xl)*[1 1],[0.30 posAx(2)],...
    'String',{'Threshold:',sprintf('%.0f mA',pSHG.b)},'color','k','HorizontalAlignment','center');
xlabel('Current (mA)')
ylabel('SHG output (mW)')
% title('Frequency doubled Nd:YAG output')
set(h,'Units','Inches');
pos = get(h,'Position');
set(h,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
print(h,'..\SHG-Output','-dpdf','-r600')
%%
fSHG2=fit(interp1(DataDiode(:,1),DataDiode(:,3),DataSHG(:,1)),DataSHG(:,2),'a*(x-b)^2','StartPoint',[1 0.3]);
x2=100:400;
y2=feval(fSHG2,x2);

figure
hold on, box on
plot(interp1(DataDiode(:,1),DataDiode(:,3),DataSHG(:,1)),DataSHG(:,2),'ks','MarkerFaceColor','k')
plot(x2,y2,'k-')
xlabel('Pump power (mW)')
ylabel('SHG output (mW)')
% title('Frequency doubled Nd:YAG output')

% figure
% loglog(DataSHG(:,1)-240,DataSHG(:,2),'ks-','MarkerFaceColor','k')
% xlabel('Current (mA)')
% ylabel('SHG output (mW)')
