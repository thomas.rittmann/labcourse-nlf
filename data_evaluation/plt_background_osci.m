x=[-0.1,-0.1];
y=[-500,1200];
figplt=figure(2);
plot(x,y);
xlim([0,1]);
ylim([-80,1080]);
xlabel('Time [ms]');
ylabel('Photodiode voltage [mV]');
grid on;
yticks([0:200:1000]);

%%

x=[-0.1,-0.1];
y=[-500,1200];
figplt=figure(2);
plot(x,y);
xlim([0,100]);
ylim([-350,1650]);
xlabel('Time [�s]');
ylabel('Photodiode voltage [mV]');
grid on;
yticks([0:400:1800]);
%%
set(0,'defaultaxesfontsize',8.5);
set(0,'defaulttextfontsize',8.5); 
x=[-12312,-1231];
y=[-500,1200];
figplt=figure(2);
plot(x,y);
xlim([-5,5]);
ylim([-9,71]);
xlabel('Time [ms]');
ylabel('Photodiode voltage [mV]');
xticks([-4,-2,0,2,4]);
yticks([0,20,40,60,80]);
grid on;
set(figplt,'Units','centimeters');
set(figplt,'Position',[5,5,8,6]);
print(figplt,'.\protocol_thomas\fig_prep\bg_shg_tot_fu','-dpng','-r600')

%%
x=[-0.1,-0.1];
y=[-500,1200];
figne=figure(300);
plot(x,y);
xlim([796,816]);
xticks([796:2:816]);
yticks([0:1:12]);
ylim([0,12]);
xlabel('wavelength \lambda [nm]');
ylabel('absorption coeff. \alpha [cm^{-1}]');
set(figne,'Units','centimeters');
set(figne,'Position',[6,6,8.5,5.8]);