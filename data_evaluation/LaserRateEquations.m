function [t,n,q]=LaserRateEquations()
b=0.01;
p=5*b;
dt=1e-1;
tmax=400;
T=[dt:dt:tmax];

[t,y]=ode45(@rateEquations,T,1e-6*[1 0]);
q=y(:,1);
n=y(:,2);

% set(0,'DefaultAxesFontSize',14,'DefaultTextFontSize',14, 'DefaultAxesFontName', 'Arial');
set(groot, 'defaultAxesTickLabelInterpreter','latex');
set(groot, 'defaultLegendInterpreter','latex');
h=figure('units','centimeters');%,'Position',[2 2 14 8]);
[AX,ax1,ax2]=plotyy(t,q,t,n);
xlabel('Time')
ylabel(AX(1),'Photon number $q$')
ylabel(AX(2),'Population difference $n$')
text(280,0.165,sprintf('$p=%.2f, b=%.2f$',p,b),'FontSize',12,'interpreter','latex')
text(340,0.015,'$q$','FontSize',12,'Color',ax1.Color,'interpreter','latex')
text(340,1.05,'$n$','FontSize',12,'Color',ax2.Color,'Parent', AX(2),'interpreter','latex')
set(ax2,'LineStyle','--')

set(h,'Units','Inches');
pos = get(h,'Position');
set(h,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
print(h,'LaserRateEq','-dpdf','-r600')

    function dy=rateEquations(t,y)
        dy=zeros(2,1);
        dy(1)=y(1)*(y(2)-1);
        dy(2)=p-b*y(2)-2*y(1)*y(2);
    end
end